# MavAppoint #
MavAppoint is an online portal through which students can book advising appointment with an Academic Advisor

#Key features of this Application#
* Students(Prospective/Transfer) and Guests can book appointment with Advisors.
* Advisors can publish their advising schedules for all the users.
* Simple and intuitive Calendar interface to book appointments.
* Up-to-date email notifications and Calendar sync.
* Instant alerts if appointments have been changed/modified.