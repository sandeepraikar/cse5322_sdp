<jsp:include page='<%=(String) request.getAttribute("includeHeader")%>' />
<%  String message = (String)request.getAttribute("message");%>
<style>
.resize {
	width: 60%;
}

.resize-body {
	width: 80%;
}
</style>
<div class="container">


	<!-- Panel -->

	<div class="panel panel-default resize center-block">

		<!-- Default panel contents -->
		<div class="panel-heading ">
		</div>
		 <label style="text-align: center" for="message"><font
			color="#0" size="4"><%=message%></font></label> <br> 
		<div class="panel-body resize-body center-block">
			<div class="text-center">
				<h3>
					<i class="fa fa-lock fa-4x"></i>
				</h3>
				<h2 class="text-center">Forgot Password?</h2>
				<p>You can reset your password here.</p>
				<div class="panel-body">

					<form class="forgotPassword" method="POST" onsubmit="return validate()">
						<fieldset>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-envelope color-blue"></i></span> <input
										id="emailId" placeholder="email address"
										name="emailId"
										class="form-control"
																				
										type="email">
								</div>
							</div>
							<div class="form-group">
								<input class="btn btn-lg btn-primary btn-block"
									value="Reset Password" type="submit">
							</div>
						</fieldset>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function validate() {
	  email =  document.getElementById("emailId").value;

	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  if(regex.test(email)){
		  return true;
	  }else{
		  alert("Please enter valid email id");
		  return false;
	  }
	}
     
</script>

<%@include file="templates/footer.jsp"%>