<jsp:include page='<%=(String) request.getAttribute("includeHeader")%>' />
<%@ page import="uta.mav.appoint.login.LoginUser"%>
<%@ page import="uta.mav.appoint.login.AdminUser"%>
<%@ page import="uta.mav.appoint.login.AdvisorUser"%>
<%@ page import="uta.mav.appoint.login.StudentUser"%>
<%@ page import="uta.mav.appoint.login.FacultyUser"%>

<% StudentUser user = (StudentUser)session.getAttribute("user"); %>
<div class="input-group-btn">
	<%@ page import="uta.mav.appoint.beans.AppointmentType"%>
	<% ArrayList<AppointmentType> ats = (ArrayList<AppointmentType>)session.getAttribute("appointmenttypes");
		if (ats != null){%>
	<button class="btn btn-default btn-lg dropdown-toggle" type="button"
		data-toggle="dropdown">
		Select Advising Type for
		<%=request.getParameter("pname")%>
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
		<%for (int i=0;i<ats.size();i++){
				%><li><a
			href="?apptype=<%=ats.get(i).getType()%>&pname=<%=request.getParameter("pname")%>&advisor_email=<%=ats.get(0).getEmail()%>&id1=<%=request.getParameter("id1")%>&duration=<%=ats.get(i).getDuration()%>"><%=ats.get(i).getType()%></a></li>
		<%
			}
		}%>
	</ul>
</div>
<br>
<br>
<hr>
<div id='calendar'></div>
<%@ page import="java.util.ArrayList"%>
<%@ page import="uta.mav.appoint.TimeSlotComponent"%>
<%@ page import="uta.mav.appoint.PrimitiveTimeSlot"%>
<%@ page import="uta.mav.appoint.CompositeTimeSlot"%>

<% TimeSlotComponent schedule = (TimeSlotComponent)session.getAttribute("timeslot");
		    			if (schedule != null){%>
<script>
		    				$(document).ready(function(){
		    					$('#calendar').fullCalendar({
		    						defaultView : 'basicDay',
		    						viewRender: function(view, element){
		    							$('#calendar').fullCalendar('gotoDate','<%=schedule.getDate()%>');
		    						},
		    						displayEventEnd : {
		    							month: false,
		    							basicWeek: false,
		    							'default' : false,
		    						}<%if(request.getParameter("duration") != null){%>,
		    						eventClick: function(event,element){
		    							document.getElementById("id2").value = event.id;
		    							document.getElementById("apptype").value = '<%=request.getParameter("apptype")%>';
		    							document.getElementById("duration").value = '<%=request.getParameter("duration")%>';
		    							document.getElementById("pname").value = '<%=request.getParameter("pname")%>';
		    							document.getElementById("advisor_email").value = '<%=request.getParameter("advisor_email")%>';
		    							document.getElementById("start").value = event.start;
		    							document.getElementById("end").value = event.end;
		    							document.getElementById("starttime").value = event.start.format();
		    							document.getElementById("endtime").value = event.end.format();
		    							
		    							$('#addApptModal').modal();
		    							},
		    					events: [
		    					    <%=schedule.getEvent(Integer.parseInt(request.getParameter("duration"))/5)%>     
		    					        ]<%}%>	
		    					});
		    				});
	 						</script>
<%}%>
<script type="text/javascript">
       function validate()
       {
       	var email = document.register_form.drp_department.value;
       	var studentId = document.register_form.drp_degreeType.value;
       	var phone = document.register_form.value;
       	
       	var valid = true;
       	
       	if(department == '' || degreeType == '' || major == '' || lastNameInitial == '')
       	  valid = false;
       	if(studentId == '' || phoneNumber == '' || email == '')
       	  valid = false;
        //Undetermined syntax exception on regex validation, no errors thrown in browser.  Server throws JSP syntax errors.
	    var phoneNumReg = '/^\(?([0-9]{3})\)?-?([0-9]{3})-?([0-9]{4})$/';
	    if(phoneNumReg.exec(phone) == '') 
	    {
	        valid = false;
	    }
	    var stuIdReg = '/^\(?([0-9]{11})\)$/';
		if(!studentId.match(stuIdReg)) 
		{
		  valid = false;
		}
      	
	    var emailReg = '/^\w+@^/mavs.uta.edu$/';
		if(!email.match(emailReg)) 
		{
		  valid = false;
		}
		
      	return valid;
       };
</script>
<br />
<br />
<hr>
<form name=addAppt role="form" action="schedule"  onsubmit="return FormSubmit()" method="post">
	<div class="modal fade" id="addApptModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="addApptTypeLabel">Add Appointment</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name=id2 id="id2"/> 
					<input type="hidden" name=apptype id="apptype"/> 
					<input type="hidden" name=start id="start"/> 
					<input type="hidden" name=end id="end"/> 
					<input type="hidden" name=starttime id="starttime"/> 
					<input type="hidden" name=endtime id="endtime"/> 
					<input type="hidden" name=pname id="pname"/>
					<input type="hidden" name=duration id="duration"/> 
					<input type="hidden" name=advisor_email id="advisor_email"/> 
					<div class="form-group">
						<label for="email">Email Address</label>
						<input type="email" placeholder="email" class="form-control" name="email" id="email" value="<%= user.getEmail()%>" data-error="email address is invalid" required>
					</div>
					<div class="form-group">
						<label for="studentid">UTA Student ID</label>
						<input type="text" placeholder="100xxxxxxx" class="form-control" name="studentid" id="studentid" value="<%= user.getStudentId() %>" required>
					</div>
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" placeholder="555-555-5555" class="form-control" name="phoneNumber" id="phoneNumber" value="<%= user.getPhoneNumber() %>" required>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea placeholder="Description" class="form-control" name="description" id="description" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<style>
#calendar {
	background-color: white;
}
</style>
<script> function FormSubmit(){
									var student_email = document.getElementById("email").value;
									var advisor_email = document.getElementById("advisor_email").value;
									var starttime = document.getElementById("starttime").value;
									
									
									
									var phoneNumber = document.getElementById("phoneNumber").value;
									
									
									
									
									var endtime = document.getElementById("endtime").value;
									
									
									
									var params = ('student_email='+student_email+'&advisor_email='+advisor_email+'&starttime='+starttime+   '&phoneNumber='+phoneNumber+   '&endtime='+endtime);
									
									
									
									
									var xmlhttp;
									xmlhttp = new XMLHttpRequest();
									xmlhttp.onreadystatechange=function(){
										
									}
									xmlhttp.open("POST","mail",true);
									xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
									xmlhttp.setRequestHeader("Content-length",params.length);
									xmlhttp.setRequestHeader("Connection","close");
									xmlhttp.send(params);
									alert("Meeting request sent.");
								}
								</script>

<%@include file="templates/footer.jsp"%>