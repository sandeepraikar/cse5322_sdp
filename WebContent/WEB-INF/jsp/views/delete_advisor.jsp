<jsp:include page='<%=(String) request.getAttribute("includeHeader")%>' />
<%@ page import="java.util.ArrayList"%>
<%
	String message = (String)request.getAttribute("message");
%>
<%
	ArrayList<String> array = (ArrayList<String>)session.getAttribute("advisors");
%>
<style>
.resize {
	width: 60%;
}

.resize-body {
	width: 80%;
}
</style>
<div class="container">


	<!-- Panel -->

	<div class="panel panel-default resize center-block">

		<!-- Default panel contents -->
		<div class="panel-heading "></div>
		<label style="text-align: center" for="message"><font
			color="#0" size="4"><%=message%></font></label> <br>
		<div class="panel-body resize-body center-block">
			<div class="text-center">
				<h3>
					<i class="fa fa-lock fa-4x"></i>
				</h3>
				<h2 class="text-center">Delete Advisor Account</h2>
				<p>Please choose the Advisor profile</p>
				<div class="panel-body">

					<form class="deleteAdvisor" method="POST"
						onsubmit="return validate()">
						<fieldset>
							<div class="form-group">
								<div class="input-group">
									
									<select id="advisorEmail" name="advisorEmail"
										class="btn btn-default btn-lg dropdown-toggle">
										<%
											for (int i=0;i<array.size();i++){
										%>
										<option id="option" value=<%=array.get(i)%>>
											<%=array.get(i)%></option>
										<%
											}
										%>
									</select>
									
								</div>
							</div>
							<div class="form-group">
								<input class="btn btn-lg btn-primary btn-block"
									value="Delete Advisor" type="submit">
							</div>
						</fieldset>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validate() {
	}
</script>

<%@include file="templates/footer.jsp"%>