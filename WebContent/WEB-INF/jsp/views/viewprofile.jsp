<jsp:include page='<%=(String) request.getAttribute("includeHeader")%>' />
<%@ page import="java.util.ArrayList"%>
<%@ page import="uta.mav.appoint.login.LoginUser"%>
<%  String message = (String)session.getAttribute("message");%>
<% LoginUser user = (LoginUser)session.getAttribute("user");%>
<% String username = user.getEmail(); %>
<% String accountType = user.getRole(); %>
<% boolean notificationsOn = user.isNotificationsOn(); %>

<style>
.resize {
width: 60%;
}
.resize-body {
width: 80%;
}


</style>
<div class="container">
	
	
<!-- Panel -->

<div class="panel panel-default resize center-block">
<form action="viewprofile" method="post">
  <!-- Default panel contents -->
  <div class="panel-heading text-center"><h1>Profile</h1></div>
  <label style="text-align: center" for="message"><font color="#0" size="4"><%=message%></font></label> <br>
  <div class="panel-body resize-body center-block">
  		
				<div class="form-group"> 
				
					<label for="username"><font color="#0" size="4">Username:  </font></label> <%=username%>
					<br>
					<label for="accounttype"><font color="#0" size="4">Type:  </font></label> <%=accountType%>
					<br>
					<label for="notifications"><font color="#0" size="4">Notifications:  </font></label> 
					<input type="radio" name="notifications" value="on">On   
					<input type="radio" name="notifications" value="of">Off
					<br>
				</div>
			
		</div>
		<div class="panel-footer text-center">
      	<input type="submit" class="btn-lg" value="Submit">
      </div>
      </form>
		</div>
		</div>


<%@include file="templates/footer.jsp"%>