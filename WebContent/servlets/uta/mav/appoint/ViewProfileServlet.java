package uta.mav.appoint;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uta.mav.appoint.db.DatabaseManager;
import uta.mav.appoint.login.LoginUser;

/**
 * Servlet implementation class ViewProfileServlet
 */
@WebServlet("/ViewProfileServlet")
public class ViewProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession session;
	String header;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		session = request.getSession();
		try{
			LoginUser user = (LoginUser)session.getAttribute("user");
			if (user == null){
				user = new LoginUser();
				session.setAttribute("user", user);
				response.sendRedirect("/WEB-INF/jsp/views/index.jsp");	
			}
			else{
				try{
					header = "templates/" + user.getHeader() + ".jsp";
	
				}
				catch(Exception e){
					System.out.printf(e.toString());
				}
			}
		}
		catch(Exception e){
			header = "templates/header.jsp";
			System.out.println(e);
		}
		session.setAttribute("message", "");
		request.setAttribute("includeHeader", header);
		request.getRequestDispatcher("/WEB-INF/jsp/views/viewprofile.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			session = request.getSession();
			LoginUser user = (LoginUser)session.getAttribute("user");
			DatabaseManager dbm = new DatabaseManager();
			String notificationsOn = request.getParameter("notificationsOn");
			String notificationsOff  = request.getParameter("notificationsOff");
			boolean notificationFinal = false;
			if(user.isNotificationsOn() != notificationFinal){
			{
				dbm.updateUser(user);

				session.setAttribute("user", user);
				session.setAttribute("message", "Profile Successfully updated!");
				request.setAttribute("includeHeader", header);
				request.getRequestDispatcher("/WEB-INF/jsp/views/viewprofile.jsp").forward(request,response);
			} else {
				session.setAttribute("user", user);
				session.setAttribute("message", "No changes made.");
				request.setAttribute("includeHeader", header);
				request.getRequestDispatcher("/WEB-INF/jsp/views/viewprofile.jsp").forward(request,response);

			}
		}
		catch(Exception e){
			session.setAttribute("message", "Error in updating profile setting.");
			e.getStackTrace();
		}
	}

}
