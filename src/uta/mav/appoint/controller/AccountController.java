package uta.mav.appoint.controller;

import uta.mav.appoint.db.DatabaseManager;
import uta.mav.appoint.email.Email;
import uta.mav.appoint.util.HashUtil;
import uta.mav.appoint.util.RandomStringGenerator;

public class AccountController {

	/**
	 * Resets the password and sends an email to the registered email-id.
	 * @param emailId
	 * @return
	 */
	public String resetPassword(String emailId) {
		String message = null;
		try {
			DatabaseManager dbm = new DatabaseManager();
			int userId = dbm.checkUserExists(emailId);
			if (userId > 0) {
				System.out.println("user id exists");
				String tempPassword = RandomStringGenerator
						.generateRandomString(8,
								RandomStringGenerator.Mode.SPECIAL);
				System.out.println("temp password generated :" + tempPassword);
				if (dbm.resetPassword(userId, emailId,
						HashUtil.getHash(tempPassword))) {
					System.out.println("Password reset successfully!!");
					Email userEmail = new Email(
							"MavAppoint Password reset successfull!",
							"Your account password has been reset!\n Your new password is : "
									+ tempPassword, emailId);
					userEmail.sendMail();
					message = "Your account password has been reset, Please check your mail!";
				} else {
					message = "Some error occured while resetting the password, please try after sometime";
				}
			} else {
				System.out.println("user id does not exist");
				message = "The user with the email id : " + emailId
						+ " does not exist";
			}

		} catch (Exception e) {
			System.out
					.println("Exception occured while resetting the password :"
							+ e.getMessage());
			e.printStackTrace();
		}
		return message;
	}
}
