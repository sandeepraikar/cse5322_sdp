package uta.mav.appoint.controller;

import uta.mav.appoint.db.DatabaseManager;

public class AdvisorController {

	public boolean deleteAdvisorAccount(String advisorEmailId) {
		boolean result= false;
		
		try{
			DatabaseManager dbm = new DatabaseManager();
			 result=dbm.deleteAdvisorAccount(advisorEmailId);
		}catch(Exception ex){
			System.out.println("Exception occured while deleting Advisor account : "+ex.getMessage());
			ex.printStackTrace();
		}
		return result;
	}

}
