package uta.mav.appoint;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uta.mav.appoint.controller.AccountController;

/**
 * Servlet implementation class ResetPasswordServlet
 */
@WebServlet("/ResetPasswordServlet")
public class ResetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	String header = null;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		header = "templates/header.jsp";
		request.setAttribute("includeHeader", header);
		request.setAttribute("message", "");
		request.getRequestDispatcher("/WEB-INF/jsp/views/forgotPassword.jsp")
				.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("Email id retrieved :"
				+ request.getParameter("emailId"));
		AccountController accountController = new AccountController();
		String message = accountController.resetPassword(request
				.getParameter("emailId"));
		if(message==null){
			message = "Your massword could not be reset. Please try later..";
		}
		header = "templates/header.jsp";
		request.setAttribute("includeHeader", header);
		request.setAttribute("message", message);
		request.getRequestDispatcher("/WEB-INF/jsp/views/forgotPassword.jsp")
				.forward(request, response);
	}

}
