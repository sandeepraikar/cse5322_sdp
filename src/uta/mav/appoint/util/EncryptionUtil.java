package uta.mav.appoint.util;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class EncryptionUtil {

	static Cipher cipher;
	private static int keySize = 128;

	public static String encryptUserId(String userId) {
		String encryptedUserId= null;
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(keySize);
			SecretKey secretKey = keyGenerator.generateKey();
			cipher = Cipher.getInstance("AES");
			
			encryptedUserId =encrypt(userId, secretKey);
			System.out.println("User Id encrypted successfully!");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedUserId;

	}

	public static String encrypt(String plainText, SecretKey secretKey)
			throws Exception {
		byte[] plainTextByte = plainText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		Base64.Encoder encoder = Base64.getEncoder();
		
		return encoder.encodeToString(encryptedByte);
		
	}

	public static String decrypt(String encryptedText, SecretKey secretKey)
			throws Exception {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] encryptedTextByte = decoder.decode(encryptedText);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		System.out.println("User Id decrypted successfully!");
		
		return decryptedText;
	}
}
