package uta.mav.appoint.util;

import java.util.Properties;

public class Configuration {
	private static Configuration _instance = null;

	private Properties props = null;

	private Configuration() {
		props = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			props.load(classLoader.getResourceAsStream("config.properties"));
		} catch (Exception e) {
			System.out
					.println("Exception occured while loading properties file :"
							+ e.getMessage());
			e.printStackTrace();
		}
	}

	public synchronized static Configuration getInstance() {
		if (_instance == null)
			_instance = new Configuration();
		return _instance;
	}

	// get property value by name
	public String getProperty(String key) {
		String value = null;
		if (props.containsKey(key))
			value = (String) props.get(key);
		else {
			System.out.println("Value not present in the properties file");
		}
		return value;
	}
}