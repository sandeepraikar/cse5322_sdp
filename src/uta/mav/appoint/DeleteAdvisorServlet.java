package uta.mav.appoint;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uta.mav.appoint.controller.AdvisorController;
import uta.mav.appoint.db.DatabaseManager;

/**
 * Servlet implementation class DeleteAdvisorServlet
 */
@WebServlet("/DeleteAdvisorServlet")
public class DeleteAdvisorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession session = null;
	String header = null;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession();

		try {
			DatabaseManager dbm = new DatabaseManager();
			ArrayList<String> array = dbm.getAdvisors();
			if (array.size() != 0) {
				session.setAttribute("advisors", array);
			}
		} catch (Exception e) {
			System.out
					.println("Exception occured while retrieving list of advisors :"
							+ e.getMessage());
			e.printStackTrace();
		}
		header = "templates/admin_header.jsp";
		request.setAttribute("includeHeader", header);
		request.setAttribute("message", "");
		request.getRequestDispatcher("/WEB-INF/jsp/views/delete_advisor.jsp")
				.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message = null;
		System.out.println("Email id retrieved :"
				+ request.getParameter("advisorEmail"));
		AdvisorController advisorController = new AdvisorController();
		boolean result = advisorController.deleteAdvisorAccount(request
				.getParameter("advisorEmail"));
		if (result == false) {
			message = "Advisor could not be deleted. Please try later..";
		} else {
			message = "Advisor account deleted successfully!!";
		}
		header = "templates/admin_header.jsp";
		request.setAttribute("includeHeader", header);
		request.setAttribute("message", message);
		request.getRequestDispatcher("/WEB-INF/jsp/views/delete_advisor.jsp")
				.forward(request, response);
	}

}
