package uta.mav.appoint.db.command;

import java.sql.PreparedStatement;

public class CheckUserExists extends SQLCmd {
	String emailId;

	public CheckUserExists(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public void queryDB() {
		try {
			String command = "SELECT userId FROM User where EMAIL=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setString(1, emailId);
			res = statement.executeQuery();
			
		} catch (Exception e) {
			System.out.println(e + " -- Found in -- "
					+ this.getClass().getSimpleName());
		}

	}

	@Override
	public void processResult() {
		int userId = 0;
		try {
			if (res.next()) {				
				userId = res.getInt(1);
				result.add(userId);
			} else {
				result.add(0);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
