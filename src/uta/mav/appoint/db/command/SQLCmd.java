package uta.mav.appoint.db.command;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import uta.mav.appoint.util.Configuration;

/*
 * SQLCmd -> implements command and template patterns
 */
public abstract class SQLCmd {
	ArrayList<Object> result = new ArrayList<Object>();
	ResultSet res;
	Connection conn;

	public ArrayList<Object> getResult() {
		return result;
	};

	public abstract void queryDB();

	public abstract void processResult();

	public void execute() {
		try {
			connectDB();
			queryDB();
			processResult();
			disconnectDB();
		} catch (Exception e) {
			disconnectDB();
		}
	}

	public void connectDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String jdbcUrl = Configuration.getInstance().getProperty(
					"mysql.jdbc.url");
			String userid = Configuration.getInstance().getProperty(
					"mysql.username");
			String password = Configuration.getInstance().getProperty(
					"mysql.password");
			conn = DriverManager.getConnection(jdbcUrl, userid, password);
			//System.out.println(conn.g);
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}

	public void disconnectDB() {
		try {
		conn.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
