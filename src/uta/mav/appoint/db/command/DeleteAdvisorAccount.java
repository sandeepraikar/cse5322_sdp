package uta.mav.appoint.db.command;

import java.sql.PreparedStatement;

public class DeleteAdvisorAccount extends SQLCmd {

	private String advisorName;
	int res =0;
	public DeleteAdvisorAccount(String advisorName) {
		super();
		this.advisorName = advisorName;
	}

	@Override
	public void queryDB() {
		try {
			String command = "DELETE FROM User where userid = (select userid from user_advisor where pName=?)";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setString(1, advisorName);			
			res = statement.executeUpdate();

		} catch (Exception ex) {
			System.out.println("Exception occured while Deleteing Advisor account from User table "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	@Override
	public void processResult() {
		result.add(res);
	}

	
}
