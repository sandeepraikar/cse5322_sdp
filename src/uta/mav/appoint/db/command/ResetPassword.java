package uta.mav.appoint.db.command;

import java.sql.PreparedStatement;

public class ResetPassword extends SQLCmd {

	private int userId;
	private String emailId;
	private String tempPassword;
	private int res = 0;
	public ResetPassword(int userId, String emailId, String tempPassword) {
		super();
		this.userId = userId;
		this.emailId = emailId;
		this.tempPassword = tempPassword;
	}

	@Override
	public void queryDB() {
		try {
			String command = "UPDATE User SET password = ?, validated = ? WHERE userId=? and email=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setString(1, tempPassword);
			statement.setInt(2, 0);
			statement.setInt(3, userId);
			statement.setString(4, emailId);
			res = statement.executeUpdate();

		} catch (Exception ex) {
			System.out.println("Exception occured while Updating User table "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	@Override
	public void processResult() {
		result.add(res);
	}

}
